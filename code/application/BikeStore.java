// 2239384 Vinicius

package application;

import vehicles.Bicycle;

public class BikeStore {

    public static void main(String[] args) {
        vehicles.Bicycle[] bicycle = new Bicycle[4];

        bicycle[0] = new Bicycle("Vasco", 21, 40.0);
        bicycle[1] = new Bicycle("Palmeiras", 18, 35.0);
        bicycle[2] = new Bicycle("Galo", 24, 45.0);
        bicycle[3] = new Bicycle("Gremio", 27, 48.0);

        System.out.println(bicycle[0]);
        System.out.println(bicycle[1]);
        System.out.println(bicycle[2]);
        System.out.println(bicycle[3]);
    }
}
