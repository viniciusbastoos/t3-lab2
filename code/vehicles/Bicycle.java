// 2239384 Vinicius

package vehicles;

//a.
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;


//b.
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    //c.
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //d.
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
    }
}


